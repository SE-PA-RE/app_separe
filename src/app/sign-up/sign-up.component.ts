import {Component,  ElementRef, OnInit, ViewChild} from '@angular/core';
import {RouterExtensions} from "nativescript-angular";
import {TouchGestureEventData} from "tns-core-modules/ui/gestures";
import {View} from "tns-core-modules/ui/core/view";
import {requestPermissions, takePicture} from "nativescript-camera";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {fromAsset} from "tns-core-modules/image-source";
import {GridLayout} from "tns-core-modules/ui/layouts/grid-layout";
import {Page} from "tns-core-modules/ui/page";
import {Visibility} from "tns-core-modules/ui/enums";
import {ValidatorCpf} from "~/app/shared/validation";
import {UserService} from "~/app/services/user.service";
import {EcoFriendValidator} from "~/app/shared/models/eco-friend.model";
import {alert} from "tns-core-modules/ui/dialogs";
import {maxDate} from "tns-core-modules/ui/date-picker";
import {maskPhone} from "~/app/shared/util";
import {TextField} from "tns-core-modules/ui/text-field";

@Component({
    selector: 'ns-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.css'],
    moduleId: module.id,
})
export class SignUpComponent implements OnInit {

    private _maxDate: Date = new Date()
    date: Date
    selectedDate: string
    dateFormatBr: string
    isFriend: boolean = true
    friendValidator: EcoFriendValidator = undefined
    formUser: FormGroup
    processing: boolean = false

    @ViewChild('btn', {static: false}) btnRef: ElementRef

    private selectDateGridLayout: GridLayout
    private overlayGridLayout: GridLayout

    private phoneTextField: TextField

    constructor(private _routerExtensions: RouterExtensions,
                private _fb: FormBuilder,
                private _page: Page,
                private _userService: UserService) {
    }

    ngOnInit() {
        this.prepareForm()
        this.selectDateGridLayout = this._page.getViewById('selectDateGridLayout')
        this.overlayGridLayout = this._page.getViewById('overlayGridLayout')
        this.phoneTextField = this._page.getViewById('phoneTextField')
        this.date = this.maxDate
    }

    // Função responsável em cadastrar o usuário
    addUser() {
        if (!this.processing && this.formUser.valid) {
            // Procedimento retira todos os caracteres no número de telefone
            this.formUser.get('phone').setValue(this.formUser.get('phone').value.toString().replace(/\D/g,''))

            this.processing = true
            if (this.isFriend) {
                this.formUser.get('declaration').setValue('')
                this.formUser.get('document').setValue('')
                this._userService.addEcoFriend(this.formUser.value)
                    .subscribe(result => {
                        this.processing = false
                        this.goBack()
                        alert({title: '', message: result['message'], okButtonText: 'OK'})
                    }, error => {
                        this.processing = false
                        if (error.status == 422)
                            this.friendValidator = error.error.data
                    })
            } else {
                //Verifica se o usuário está enviando a declaração e documento
                if (this.formUser.get('declaration').value == '' || this.formUser.get('document').value == '') {
                    alert({title: '', message: 'É necessário o envio da declaração e documento com foto.', okButtonText: 'OK'})
                } else {
                    this._userService.addEcoRecycler(this.formUser.value)
                        .subscribe(result => {
                            this.processing = false
                            this.resetForm()
                            alert({title: '', message: result['message'], okButtonText: 'OK'})
                                .then(() => this.goBack())
                        }, error => {
                            this.processing = false
                            if (error.status == 422)
                                this.friendValidator = error.error.data.user
                        })
                }
            }
        } else {
            alert({title: '', message : 'Verifique se todos os campos foram preenchidos corretamente', okButtonText: 'OK'})
        }

    }

    getInfo() {
        alert("Ainda não implementado")
    }

    // Volta para tela anterior
    goBack() {
        this._routerExtensions.backToPreviousPage();
    }

    setToFriend() {
        this.isFriend = true
    }

    setToRecycler() {
        this.isFriend = false
    }

    // Ao clicar o botão a função é chamada, responsável em alterar o tamanho do botão
    onFocus(args: TouchGestureEventData) {
        if (args.action == "down") {
            this.btnRef.nativeElement.scaleX = 0.9;
            this.btnRef.nativeElement.scaleY = 0.9;
        } else if (args.action == "up") {
            this.btnRef.nativeElement.scaleX = 1;
            this.btnRef.nativeElement.scaleY = 1;
        }
    }

    // Pedido de autorização para acesso a Camera
    onRequestPermissions() {
        requestPermissions()

    }

    // Função responsável para captura da declaração, a mesma é chamada ao clicar na camera
    captureDeclaration(){
        this.onRequestPermissions()
        let options = {
            width: 250,
            height: 300,
            keepAspectRatio: true,
            saveToGallery: false
        }

        takePicture(options)
            .then(image => {
                fromAsset(image)
                    .then(img => {
                        let base64 = img.toBase64String("jpeg", 100)
                        this.formUser.get('declaration').setValue(base64)
                    })

            }).catch( err => {
                console.log(err.message)
            })
    }

    // Função que captura documento com foto, a mesma é chamada ao clicar na camera
    captureDocument() {
        this.onRequestPermissions()
        let options = {
            width: 250,
            height: 300,
            keepAspectRatio: true,
            saveToGallery: false
        }

        takePicture(options)
            .then(image => {
                fromAsset(image)
                    .then(img => {
                        let base64 = img.toBase64String("jpeg", 100)
                        this.formUser.get('document').setValue(base64)
                    })
            }).catch( err => {
            console.log(err.message)
        })
    }

    // Função que abri a modal selecionar data
    onOpenSelectDate() {
        this.selectDateGridLayout.visibility = <any>Visibility.visible
        this.selectDateGridLayout.className = 'select-date animate-bounceInUp-delay-0ms'
        this.overlayGridLayout.animate({opacity: 0.5, duration: 300})
    }

    // Função que atualiza a data
    onDateChanged(args) {
        let date: Date = args.value
        let dd = date.getDate()
        let mm = date.getMonth() + 1
        let yyyy = date.getFullYear()
        this.selectedDate = `${yyyy}-${mm<10 ? '0'+ mm : mm}-${dd<10? '0'+ dd : dd}`
    }

    // Função que atualiza os valores da data e manipula a modal de datepicker
    onCloseSelectDate(isCancel: boolean) {
        if (!isCancel) {
            this.formUser.get('date_of_birth').setValue(this.selectedDate)
            let date = this.selectedDate.split('-')
            this.dateFormatBr = `${date[2]}/${date[1]}/${date[0]}`
        }

        this.selectDateGridLayout.className = 'select-date'
        this.overlayGridLayout.animate({opacity: 0, duration: 300})
            .then(() => {
                this.selectDateGridLayout.visibility = <any>Visibility.collapse
            })
            .catch(() => {})
    }

    // Função que prepara o formulário de cadastro
    prepareForm() {
        this.formUser = this._fb.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            cpf: ['', Validators.required],
            date_of_birth: ['', Validators.required],
            phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(14)]],
            password: ['', [Validators.required, Validators.minLength(6)]],
            password_confirmation: ['', Validators.required],
            declaration: [''],
            document: [''],
        }, {validators: [this.passwordConfirmationValidator,this.cpfValidator]})
    }

    resetForm(){
        this.formUser.patchValue({
            first_name: '',
            last_name: '',
            email: '',
            cpf: '',
            date_of_birth: '',
            phone: '',
            password: '',
            password_confirmation: '',
            declaration: '',
            document: '',
        })
    }

    // Função que valida se a senha confere
    passwordConfirmationValidator(form: FormGroup) {
        if (form.get('password').value === form.get('password_confirmation').value) {
            form.get('password_confirmation').setErrors(null)
        } else {
            form.get('password_confirmation').setErrors({'passwordConfirmation': true})
        }
    }

    // Função que valida o cpf
    cpfValidator(form: FormGroup) {
        if (ValidatorCpf.cpf(form.get('cpf').value)) {
            form.get('cpf').setErrors(null)
        } else {
            form.get('cpf').setErrors({'cpfInvalid': true})
        }
    }

    // Funcao de retorno da variavel maxDate
    public get maxDate() {
        let day = new Date()
        return new Date(day.getFullYear() - 18, day.getMonth(),day.getDate())
    }

    // Limpa os erros de validação
    clearValidator() {
        this.friendValidator = undefined
    }

    onBlurPhone(args){
        this.formUser.get('phone').setValue(maskPhone(this.formUser.get('phone').value.toString(), "(99)09999-9999"))
    }
}
