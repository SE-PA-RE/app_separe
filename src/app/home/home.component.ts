import {Component, OnInit} from "@angular/core";
import {UserService} from "~/app/services/user.service";
import {getString} from "tns-core-modules/application-settings";


@Component({
    selector: "ns-home",
    templateUrl: "./home.component.html",
    moduleId: module.id
})
export class HomeComponent {

    is_recycler: boolean = true;
    first_name: string

    constructor(private _userService: UserService){
        this.is_recycler = !getString("eco_friend")
        this.first_name = getString("first_name")
    }

}
