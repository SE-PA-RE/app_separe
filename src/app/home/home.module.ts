import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptCommonModule} from "nativescript-angular/common";
import {RecyclerComponent} from "~/app/home/recycler/recycler.component";
import {FriendComponent} from "~/app/home/Friend/friend.component";
import {HomeComponent} from "~/app/home/home.component";
import {NativeScriptRouterModule} from "nativescript-angular";
import {FloatBtnComponent} from "~/app/shared/float-btn.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptRouterModule
    ],
    declarations: [
        RecyclerComponent,
        FriendComponent,
        HomeComponent,
        FloatBtnComponent
    ],
    schemas: [
       NO_ERRORS_SCHEMA
    ]
})

export class HomeModule {

}
