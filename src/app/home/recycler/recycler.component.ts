import {Component, Input, OnInit,} from '@angular/core';
import {Page} from "tns-core-modules/ui/page";
import {RouterExtensions} from "nativescript-angular";
import {LoginService} from "~/app/services/login.service";


@Component({
    selector: 'ns-recycler',
    templateUrl: './recycler.component.html',
    styleUrls: ['./recycler.component.css'],
    moduleId: module.id,
})
export class RecyclerComponent implements OnInit {

    @Input() first_name: string

    constructor(private _page: Page,
                private _routerExtensions: RouterExtensions,
                private _loginService: LoginService) {
    }

    ngOnInit() {
        this._page.actionBarHidden = true
    }

    logout() {
        this._loginService.logout()
    }

    onFocus(args) {
        let image = args.object
        if (args.action == "down") {
            image.scaleX = 0.9;
            image.scaleY = 0.9;
        } else if (args.action == "up") {
            image.scaleX = 1;
            image.scaleY = 1;
        }
    }
}
