import {ErrorHandler, NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { httpInterceptorProviders } from "~/app/http-interceptors/interceptors";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { AppRoutingModule } from "./app-routing.module";
import {HomeModule} from "~/app/home/home.module";

import { AppComponent } from "./app.component";
import { LoginComponent } from "~/app/login/login.component";
import { SignUpComponent } from './sign-up/sign-up.component';
import {AppErrorHandler} from "~/app/app-error-handler";
import {isIOS} from "tns-core-modules/platform";

// Plugins externos
// import { setStatusBarColors } from "~/app/shared";
// setStatusBarColors()

// Config google maps
declare var GMSServices: any;
if(isIOS) {
    GMSServices.provideAPIKey("AIzaSyCPXWz1edV9lQS9la12YmiAE2ZeaQyCJDU");
}


@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        HomeModule,
        ReactiveFormsModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        SignUpComponent,
    ],
    providers: [
        httpInterceptorProviders,
        // [{provide: ErrorHandler, useClass: AppErrorHandler}]
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
