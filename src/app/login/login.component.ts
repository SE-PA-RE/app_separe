import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Page, View } from "tns-core-modules/ui/page";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { TouchGestureEventData } from "tns-core-modules/ui/gestures";
import { RouterExtensions } from "nativescript-angular";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LoginService } from "~/app/services/login.service";
import { UserService } from "~/app/services/user.service";
import { TokenService } from "~/app/services/token.service";
import { setString } from "tns-core-modules/application-settings";
import { hideLoader, showLoader } from "~/app/shared/util";

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    processing: boolean = false; // Requisição está sendo processada?
    formLogin: FormGroup

    @ViewChild('password', { static: false }) password: ElementRef;
    @ViewChild('btn', { static: false }) btnRef: ElementRef;

    /**
     * Cria uma instância do LoginComponent.
     * @param {Page} _page
     * @param {RouterExtensions} _routerExtensions
     * @param {FormBuilder} _fb
     * @param {LoginService} _login
     * @param {UserService} _userService
     * @param {TokenService} _tokenService
     * @memberof LoginComponent
     */
    constructor(private _page: Page,
        private _routerExtensions: RouterExtensions,
        private _fb: FormBuilder,
        private _login: LoginService,
        private _userService: UserService,
        private _tokenService: TokenService) {
        this._page.actionBarHidden = true;
    }

    /**
     * Inicia ao carregar o component
     * 
     * @memberof LoginComponent
     */
    ngOnInit(): void {
        this.prepareForm()
    }

    /**
     * Obtem token e realiza autenticação no webservice
     * 
     * @memberof LoginComponent
     */
    login() {
        showLoader('Autenticando...')

        let email = this.formLogin.get('email').value
        let password = this.formLogin.get('password').value
        this._login.getToken(email, password)
            .subscribe((result) => {
                this._userService.getMyProfile()
                    .subscribe(
                        result => {
                            hideLoader()
                            if (result['data'].eco_friend)
                                setString("eco_friend", result['data'].eco_friend.toString())

                            setString("first_name", result['data'].first_name)
                            this.navigateHome()
                        },
                        error => {
                            hideLoader()
                            alert({ title: "", message: error.error.message, okButtonText: 'OK' })
                            this._login.logout()
                        }
                    )
            }, error => {
                hideLoader()
                alert({ title: '', message: "Credenciais inválidas", okButtonText: 'OK' })
            })

    }

    /**
     * Abri modal para resetar o password
     *
     * @memberof LoginComponent
     */
    forgotPassword() {
        prompt({
            title: "Esqueceu a Senha",
            message: "Digite o endereço de e-mail que você usou para se inscrever no SE-PA-RE para redefinir sua senha.",
            inputType: "email",
            defaultText: "",
            okButtonText: "Ok",
            cancelButtonText: "Cancel"
        }).then((data) => {
            if (data.result && data.text != '') {
                this.processing = true
                this._login.resetPassword(data.text)
                    .subscribe((result) => {
                        this.processing = false
                        if (result) {
                            alert({ title: '', message: result['message'], okButtonText: 'OK' })
                        } else {
                            alert({
                                title: '',
                                message: 'Não existe cadastro com email informado.',
                                okButtonText: 'OK'
                            })
                        }
                    }, (error) => {
                        this.processing = false
                        alert({ title: '', message: error.error['message'], okButtonText: 'OK' })
                    })
            }
        });
    }

    /**
     * Focus input password
     *
     * @memberof LoginComponent
     */
    focusPassword() {
        this.password.nativeElement.focus();
    }

    /**
     * On focus botão entrar
     *
     * @param {TouchGestureEventData} args
     * @memberof LoginComponent
     */
    onFocus(args: TouchGestureEventData) {
        if (args.action == "down") {
            this.btnRef.nativeElement.scaleX = 0.9;
            this.btnRef.nativeElement.scaleY = 0.9;
        } else if (args.action == "up") {
            this.btnRef.nativeElement.scaleX = 1;
            this.btnRef.nativeElement.scaleY = 1;
        }
    }

    /**
     * Função que navega para tela home.
     *
     * @private
     * @memberof LoginComponent
     */
    private navigateHome() {
        this._routerExtensions.navigate(['home'], {
            clearHistory: true,
            animated: true,
            transition: {
                name: 'fade',
                duration: 500,
                curve: 'ease'
            }
        })
    }

    /**
     * Prepara o formulário de login
     *
     * @memberof LoginComponent
     */
    prepareForm() {
        this.formLogin = this._fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        })
    }
}
