import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import {LoginComponent} from "~/app/login/login.component";
import {SignUpComponent} from "~/app/sign-up/sign-up.component";
import {AppAuthGuard} from "~/app/app-auth-guard";

import {HomeComponent} from "~/app/home/home.component";


const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'sign-up', component: SignUpComponent },
    { path: 'home', component: HomeComponent, canActivate: [AppAuthGuard] },
    { path: 'pages', loadChildren: '~/app/pages/pages.module#PagesModule', canActivate: [AppAuthGuard]},

];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
