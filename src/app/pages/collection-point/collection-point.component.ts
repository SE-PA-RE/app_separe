import {Component, OnInit, ViewChild} from '@angular/core';
import {registerElement, RouterExtensions} from "nativescript-angular";
import {PointService} from "~/app/services/point.service";
import {Point} from "~/app/shared/models/point.model";
import {MapView, Marker, Position} from "nativescript-google-maps-sdk";
import {enableLocationRequest, getCurrentLocation, isEnabled} from "nativescript-geolocation";
import {Accuracy} from "tns-core-modules/ui/enums";

registerElement('MapView', () => MapView);

@Component({
    selector: 'ns-collection-point',
    templateUrl: './collection-point.component.html',
    styleUrls: ['./collection-point.component.css'],
    moduleId: module.id,
})
export class CollectionPointComponent implements OnInit {

    latitude: number = -10.191116991699444
    longitude: number = -48.330207914114006
    zoom: number = 14
    minZoom: number = 0
    maxZoom: number = 22
    bearing = 0
    tilt = 0
    padding = [40, 40, 40, 40]
    mapView: MapView
    processing: boolean = false
    points: Point[] = []
    lastCamera: String;

    constructor(private _routerExtensions: RouterExtensions,
                private _pointService: PointService) {
    }

    ngOnInit() {
        isEnabled().then(res => {
            if(res) {
               this.currentLocation()
            } else {
                this.request()
            }
        }, error => {
            this.request()
        })
    }

    // Chamado quando o Google Map está pronto para uso
    onMapReady(event) {
        console.log('Map Ready');

        this.mapView = event.object;

        console.log("Setting a marker...");

        this._pointService.getPoints()
            .subscribe(result => {
                this.points = result['data']
                console.log(this.points)
                if (this.points.length > 0) {
                    for (let i in this.points){
                        let marker = new Marker();
                        marker.position = Position.positionFromLatLng(this.points[i].location.latitude, this.points[i].location.longitude);
                        marker.title = this.points[i].name
                        marker.snippet = this.points[i].description
                        marker.userData= {index: this.points[i].id}
                        this.mapView.addMarker(marker)
                    }
                }
            })
    }

    request() {
        enableLocationRequest().then(res => {
            this.currentLocation()
        }, error => {
            console.log(error)
        })
    }

    currentLocation(): void {
        this.processing = true
        getCurrentLocation({
            desiredAccuracy: Accuracy.high,
            timeout: 5000
        }).then(res => {
            //this.latitude = res.latitude
            //this.longitude = res.longitude
            console.log(res)
            //    fetch(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${res.latitude}&lon=${res.longitude}&zoom=18&addressdetails=1&accept-language=pt-BR`)
            //        .then(res => res.json(),error => this.processing = false).then((r) => {
            //            this.formAddress.get('city').setValue(r['address'].city)
            //            this.formAddress.get('cep').setValue(r['address'].postcode)
            //            this.formAddress.get('sector').setValue(r['address'].neighbourhood)
            //            this.formAddress.get('uf').setValue(r['address'].state)
            //            this.processing = false
            //    })
            this.processing = false
        }).catch(error => {
            console.log("Location error received" + error)
            this.processing = false
        })
    }

    onCoordinateTapped(args) {
        console.log("Coordinate Tapped, Lat: " + args.position.latitude + ", Lon: " + args.position.longitude, args);
    }

    onMarkerEvent(args) {
        console.log("Marker Event: '" + args.eventName
            + "' triggered on: " + args.marker.title
            + ", Lat: " + args.marker.position.latitude + ", Lon: " + args.marker.position.longitude, args);
    }

    onCameraChanged(args) {
        console.log("Camera changed: " + JSON.stringify(args.camera), JSON.stringify(args.camera) === this.lastCamera);
        this.lastCamera = JSON.stringify(args.camera);
    }

    onCameraMove(args) {
        console.log("Camera moving: " + JSON.stringify(args.camera));
    }
}
