import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from "@angular/core";
import {Switch} from "tns-core-modules/ui/switch";
import {TypeMaterial} from "~/app/shared/models/type-material.model";
import { MaterialService } from "~/app/services/material.service";

class Material{
    id: number
    type: string
    description: string
    check: boolean
}

@Component({
    selector: 'ns-collections-form',
    template: `
        <AbsoluteLayout>
            <ScrollView class="card-content">
                <StackLayout  class="body card">
                    <GridLayout *ngFor="let type of types" rows="auto" columns="*, auto" class="m-10">
                        <Label row="0" col="0" [text]="type.type" class="h5 m-15"></Label>
                        <Switch row="0" col="1" (checkedChange)="onChecked($event, type)" class="switch" [checked]="type.check"></Switch>
                    </GridLayout>
                </StackLayout>
            </ScrollView>
            <StackLayout class="card-title">
                <Label text="O que será coletado?" horizontalAlignment="center" class="h4"></Label>
            </StackLayout>
        </AbsoluteLayout>
        <ActivityIndicator [busy]="processing" class="activity-indicator"></ActivityIndicator>
    `,
    styles: [
        `
            .body {
                background-color: #ffffff;
            }

            .card {
                margin-top: 28;
                border-radius: 10;
                padding: 50 0 24 0;
                align-items: center;
            }

            .card-title {
                width: 94%;
                height: 100px;
                margin-left: 3%;
                /*background-color: #b72806;*/
                background-color: #fff621;
                opacity: 0.9;
                color: black;
                vertical-align: center;
                font-weight: bold;
            }

            .card-content {
                height: 100%;
                width: 100%
            }
        `
    ]
})


export class CollectionsFormComponent implements OnInit {

    processing:boolean = false
    @Input() selectedType: TypeMaterial[] = []
    types: Array<Material>
    @Output() getType: EventEmitter<TypeMaterial[]> = new EventEmitter<TypeMaterial[]>()

    constructor(private _materialService: MaterialService){}

    ngOnInit(): void {
        this.getTypeMaterial()      
    }

    ngOnChanges(changes: SimpleChanges): void {
        //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
        //Add '${implements OnChanges}' to the class.
    }

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.getType.emit(this.selectedType)
    }

    getTypeMaterial() {
        this.processing = true
        this._materialService.getTypes()
        .subscribe(result => {
            this.types = result['data']
            this.types.map(t => t.check=false)
            for (let id in this.selectedType) {
            this.types.map(t => {
                if (t.id == this.selectedType[id].id){
                    return t.check = true
                }
            })
            }
            this.processing = false
        });
    }

    onChecked(args, type: TypeMaterial) {
        let selectSwitch = <Switch>args.object
        if (selectSwitch.checked) {
            let typeMat = this.selectedType.filter(t => t.id==type.id)
            if(typeMat.length==0)
                this.selectedType.push(type)
        } else {
            this.selectedType = this.selectedType.filter(t => t.id !== type.id)
        }
    }
}
