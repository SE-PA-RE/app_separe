import {Component, Input, OnInit, OnDestroy, Output, EventEmitter, ViewContainerRef, ViewChild, ElementRef} from "@angular/core";
import {Accuracy} from "tns-core-modules/ui/enums";
import { Address } from "~/app/shared/models/address.model";
import { UserService } from "~/app/services/user.service";
import {Switch} from "tns-core-modules/ui/switch";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { SearchCityComponent } from "~/app/shared/search-city.component";
import {enableLocationRequest, getCurrentLocation, isEnabled} from "nativescript-geolocation";

@Component({
    selector: 'ns-address-form',
    template: `
        
        <AbsoluteLayout class="animate-bounceInRight-delay-0ms">
            <ScrollView class="card-content">               
                <StackLayout  class="body card">
                    <GridLayout rows="auto, *" columns="*, auto" class="m-10">
                        <Label row="0" col="0"  class="h4" text="Usar meu endereço"></Label>
                        <Switch row="0" col="1" (checkedChange)="onChecked($event)"></Switch>
                    </GridLayout>
                    
                    <StackLayout [formGroup]="formAddress"  class="form" row="1" col="0" colSpan="2" padding="10">
                        <StackLayout class="form-input">
                            <TextField hint="Cep" class="text-field" formControlName="cep"></TextField>
                            <Label *ngIf="formAddress.get('cep').invalid && (formAddress.get('cep').dirty || formAddress.get('cep').touched)"
                                   class="text-danger text-center" text="Cep é obrigatório" textWrap="true"></Label>
                        </StackLayout>
                        <StackLayout class="form-input" marginTop="10">
                            <TextField hint="Logradouro" class="text-field" formControlName="logradouro"></TextField>
                            <Label *ngIf="formAddress.get('logradouro').invalid && (formAddress.get('logradouro').dirty || formAddress.get('logradouro').touched)"
                                   class="text-danger text-center" text="Logradouro é obrigatório" textWrap="true"></Label>
                        </StackLayout>
                        <StackLayout class="form-input" marginTop="10">
                            <TextField hint="Descrição" class="text-field" formControlName="description"></TextField>
                        </StackLayout>
                        <StackLayout class="form-input" marginTop="10">
                            <TextField hint="Número" class="text-field" formControlName="number"></TextField>
                            <Label *ngIf="formAddress.get('number').invalid && (formAddress.get('number').dirty || formAddress.get('number').touched)"
                                   class="text-danger text-center" text="Número é obrigatório" textWrap="true"></Label>
                        </StackLayout>
                        <StackLayout class="form-input" marginTop="10">
                            <TextField hint="Bairro" class="text-field" formControlName="sector"></TextField>
                            <Label *ngIf="formAddress.get('sector').invalid && (formAddress.get('sector').dirty || formAddress.get('sector').touched)"
                                   class="text-danger text-center" text="Bairro é obrigatório" textWrap="true"></Label>
                        </StackLayout>
                        <StackLayout class="form-input" marginTop="10">
                            <TextField #city hint="Cidade" class="text-field" [editable]="False" 
                            (tap)="onOpenSearchCity()"></TextField>
                            <Label *ngIf="formAddress.get('city').invalid && (formAddress.get('city').dirty || formAddress.get('city').touched)"
                                   class="text-danger text-center" text="Cidade é obrigatório" textWrap="true"></Label>
                        </StackLayout>
                    </StackLayout>                    
                </StackLayout>
            </ScrollView>
            <StackLayout class="card-title">
                <Label text="Qual endereço da coleta?" horizontalAlignment="center"></Label>
            </StackLayout>
        </AbsoluteLayout>
        <ActivityIndicator [busy]="processing" class="activity-indicator"></ActivityIndicator>
    `,
    styles: [
        `            
            .body {
                background-color: #ffffff;
            }
            .card {
                margin-top: 28;
                border-radius: 10;
                padding: 50 0 24 0;
                align-items: center;
            }
            .card-title {
                width: 94%;
                height: 100px;
                margin-left: 3%;
                background-color: #b72806;
                opacity: 0.9;
                color: #ffffff;
                vertical-align: center;
                font-size: 15;
                font-weight: bold;
            }
            .card-content{ height: 100%; width: 100%}
        `
    ],
    providers: [ModalDialogService]
})
export class AddressFormComponent implements OnInit, OnDestroy {

    lat = 0
    lon = 0
    speed = 0
    processing = false
    @Input() status
    @Input() setAddress: Address
    @Output() getAddress = new EventEmitter<any>()
    formAddress: FormGroup

    @ViewChild('city', {static: false}) _textFieldElement: ElementRef
    private _cityTextField: TextField

    constructor(private _userService: UserService,
                private _fb: FormBuilder,
                private _modalService: ModalDialogService,
                private _vcRef: ViewContainerRef){

                }

    ngOnInit(): void {
        this._cityTextField = <TextField>this._textFieldElement.nativeElement
        this.prepareForm()
        this.rebuildForm()
        isEnabled().then(res => {
            if(res) {
               this.currentLocation()
            } else {
                this.request()
            }
        }, error => {
            this.request()
        })
    }

    ngOnChanges() {
        let data = {
            address: this.formAddress.value,
            location: {latitude: this.lat, longitude: this.lon}
        }
        this.getAddress.emit(data)
    }

    ngOnDestroy(): void {

    }

    onChecked(args) {
        let checked = <Switch>args.object
        if (checked.checked) {
            this._userService.getMyAddress()
            .subscribe(result => {
                this.formAddress.setValue({
                    logradouro: result['data'].logradouro,
                    description: result['data'].description,
                    number: result['data'].number,
                    sector: result['data'].sector,
                    city: result['data'].city,
                    uf: result['data'].uf,
                    cep: result['data'].cep,
                })
                this._cityTextField.text = result['data'].city + " - " + result['data'].uf
            })
        } else {
           this.formAddress.reset()
        }
    }

    request() {
        enableLocationRequest().then( res => {
            this.currentLocation()
        }, error => {
            console.log(error)
        })
    }

    currentLocation(): void {
        this.processing = true
       getCurrentLocation({
           desiredAccuracy: Accuracy.high,
           timeout: 5000
       }).then(res => {
           this.lat = res.latitude
           this.lon = res.longitude
           console.log(res)
        //    fetch(`https://nominatim.openstreetmap.org/reverse?format=json&lat=${res.latitude}&lon=${res.longitude}&zoom=18&addressdetails=1&accept-language=pt-BR`)
        //        .then(res => res.json(),error => this.processing = false).then((r) => {
        //            this.formAddress.get('city').setValue(r['address'].city)
        //            this.formAddress.get('cep').setValue(r['address'].postcode)
        //            this.formAddress.get('sector').setValue(r['address'].neighbourhood)
        //            this.formAddress.get('uf').setValue(r['address'].state)
        //            this.processing = false
        //    })
        this.processing = false
       }).catch(error => {
           console.log("Location error received" + error)
           this.processing = false
       })
    }

    prepareForm() {
        this.formAddress = this._fb.group({
            logradouro: ['', Validators.required],
            description: [''],
            number: ['', Validators.required],
            sector: ['', Validators.required],
            city: ['', Validators.required],
            uf: ['', Validators.required],
            cep: ['', Validators.required],
        })
    }

    rebuildForm() {
        if (this.setAddress.logradouro !== "") {
            this.formAddress.setValue({
                logradouro: this.setAddress.logradouro,
                description: this.setAddress.description,
                number: this.setAddress.number,
                sector: this.setAddress.sector,
                city: this.setAddress.city,
                uf: this.setAddress.uf,
                cep: this.setAddress.cep,
            })
            this._cityTextField.text = this.setAddress.city + " - " + this.setAddress.uf
        }
    }

    onOpenSearchCity() {
        const options: ModalDialogOptions = {
            viewContainerRef: this._vcRef,
            context: {},
            fullscreen: true
        }

        this._modalService.showModal(SearchCityComponent, options)
            .then( (result: any) => {
                this.formAddress.get('city').setValue(result['city'].name)
                this.formAddress.get('uf').setValue(result['city'].uf)
                this._cityTextField.text = result['city'].name + " - " + result['city'].uf
            })
    }
}
