import {Component, OnInit} from '@angular/core';
import {RouterExtensions} from "nativescript-angular";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Address } from '~/app/shared/models/address.model';
import { CollectionService } from '~/app/services/collection.service';
import { showLoader, hideLoader } from '~/app/shared/util';

@Component({
    selector: 'ns-order-create',
    templateUrl: './request-create.component.html',
    styleUrls: ['./request-create.component.css'],
    moduleId: module.id,
})
export class RequestCreateComponent implements OnInit {

    currentStep: number = 1;
    status: number = 1;
    formCollect: FormGroup;

    constructor(private _routerExtensions: RouterExtensions,
                private _fb: FormBuilder,
                private _collectionService: CollectionService) {
    }

    ngOnInit() {
        this.prepareForm();
    }

    goBack() {
        this._routerExtensions.back()
    }

    setType(types: any[]) {
        const typeFGs = types.map(type => this._fb.group({id: type.id,type: type.type}))
        this.formCollect.setControl('collects', this._fb.array(typeFGs))
    }

    setAddress(data: any) {
        this.formCollect.get('address').setValue({
            logradouro: data.address.logradouro,
            description: data.address.description,
            number: data.address.number,
            sector: data.address.sector,
            city: data.address.city,
            uf: data.address.uf,
            cep: data.address.cep,
        })
        this.formCollect.get('point.model.ts').setValue({
            latitude: data.point.latitude,
            longitude: data.point.longitude
        })
    }

    setImg(imgBase64: string) {
        this.formCollect.get('image').setValue(imgBase64)
    }

    next() {
        if (this.currentStep < 4)
            switch (this.currentStep) {
                case 1:
                    this.status++
                    let value = this.formCollect.get('collects').value as FormArray
                    if(value.length == 0) {
                        alert('Precisa selecionar o que será coletado.')
                    } else {
                        this.currentStep++
                    }
                    break
                case 2:
                    this.status++
                    setTimeout(() => {
                        if (this.formCollect.get('address').valid) {
                            this.currentStep++ 
                        } else {
                            alert('Endereço incompleto.')
                        }                           
                    },100)                     
                    break
                case 3:                   
                    if (this.formCollect.get('image').valid) {
                        showLoader('Salvando ...')
                        this._collectionService.addCollection(this.formCollect.value)
                                .subscribe(result => {                                    
                                    hideLoader()
                                    this.goBack()
                                })

                    } else {
                        alert('Tire uma foto do material.')
                    }  
                    break
            }
           
    }

    back() {        
        if (this.currentStep > 1) {
            this.currentStep--
        } else if (this.currentStep === 1) {
            this.goBack()
        }           
    }

    prepareForm() {
        this.formCollect = this._fb.group({
            collects: this._fb.array([]),
            address: this._fb.group({
                logradouro: ['', Validators.required],
                description: [''],
                number: ['', Validators.required],
                sector: ['', Validators.required],
                city: ['', Validators.required],
                uf: ['', Validators.required],
                cep: ['', Validators.required],
            }),
            location: this._fb.group({
                longitude: ['2222222', Validators.required],
                latitude: ['33333333', Validators.required]
            }),
            image: ['', Validators.required]
        })
    }

}
