import {Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef} from "@angular/core";
import { requestPermissions, takePicture } from "nativescript-camera";
import { fromAsset } from "tns-core-modules/image-source/image-source";
import { isAndroid } from "tns-core-modules/ui/page/page";

@Component({
    selector: 'ns-photo-form',
    template: `
        <AbsoluteLayout class="animate-bounceInRight-delay-0ms">
            <ScrollView class="card-content">
                <StackLayout  class="body card">
                    <StackLayout padding="10" horizontalAlignment="center">
                        <Label text="&#xf030;" class="fas fa-camera" (tap)=capturePhoto()></Label>                                                                                    
                    </StackLayout>
                    <StackLayout horizontalAlignment="center">
                        <Image [src]="photoPath" width="250" height="300"></Image>
                    </StackLayout>
                </StackLayout>
            </ScrollView>
            <StackLayout class="card-title">
                <Label text="Nos envie uma foto dos resíduos" horizontalAlignment="center"></Label>
            </StackLayout>
        </AbsoluteLayout>
    `,
    styles: [
        `            
            .body {
                background-color: #ffffff;
            }
            .card {
                margin-top: 28;
                border-radius: 10;
                padding: 50 0 24 0;
            }
            .card-title {
                width: 94%;
                height: 100px;
                margin-left: 3%;
                background-color: #b72806;
                opacity: 0.9;
                color: #ffffff;
                vertical-align: center;
                font-size: 15;
                font-weight: bold;
            }
            .card-content{ height: 100%; width: 100%}
            .fa-camera {
                font-size: 50;
            }
        `
    ]
})
export class PhotoFormComponent implements OnInit{

    @Input() setImg: string
    @Output() getImg = new EventEmitter<string>()

    private photoPath: string = ""

    ngOnInit() {
        requestPermissions()
    }

    capturePhoto() {
        let options = {
            width: 250,
            height: 300,
            keepAspectRatio: true,
            saveToGallery: false
        }

        takePicture(options) 
        .then(image => {
            if(isAndroid)
                this.photoPath = image['_android']

            fromAsset(image)
            .then(img => {
                let base64 = img.toBase64String("jpeg", 100)
                //Aqui emitir o evento para o outro component
                this.getImg.emit(base64)
            })
        })
    }
    
}
