import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptCommonModule} from "nativescript-angular/common";
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";

import {PagesRoutingModule} from "~/app/pages/pages.routing.module";

import {RequestsComponent} from "~/app/pages/requests/requests.component";
import {RequestDetailComponent} from "~/app/pages/request-detail/request-detail.component";
import {ProfileComponent} from "~/app/pages/profile/profile.component";
import {TypeMaterialListComponent} from "~/app/pages/type-material-list/type-material-list.component";
import {RequestCreateComponent} from "~/app/pages/request-create/request-create.component";
import {CollectionsFormComponent} from "~/app/pages/request-create/collections-form.component";
import {AddressFormComponent} from "~/app/pages/request-create/address-form.component";
import {PhotoFormComponent} from "~/app/pages/request-create/photo-form.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NativeScriptFormsModule} from "nativescript-angular";
import { SearchCityComponent } from "../shared/search-city.component";
import {CollectionPointComponent} from "~/app/pages/collection-point/collection-point.component";


@NgModule({
    imports: [
        PagesRoutingModule,
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        NativeScriptFormsModule,
        ReactiveFormsModule,
        FormsModule
    ],
    declarations: [
        RequestsComponent,
        RequestDetailComponent,
        ProfileComponent,
        TypeMaterialListComponent,
        RequestCreateComponent,
        CollectionsFormComponent,
        AddressFormComponent,
        PhotoFormComponent,
        SearchCityComponent,
        CollectionPointComponent
    ],
    entryComponents: [
        SearchCityComponent
    ],
    schemas:[
        NO_ERRORS_SCHEMA
    ]
})

export class PagesModule{}
