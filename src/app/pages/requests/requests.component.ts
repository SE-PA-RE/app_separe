import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {RouterExtensions, SetupItemViewArgs} from "nativescript-angular";
import {SearchBar} from "tns-core-modules/ui/search-bar";
import {Request} from "~/app/shared/models/request.model";
import {isAndroid} from "tns-core-modules/platform";
import {
    ListViewEventData,
    ListViewLinearLayout,
    ListViewScrollDirection,
    LoadOnDemandListViewEventData,
    RadListView
} from "nativescript-ui-listview";
import {ObservableArray} from "tns-core-modules/data/observable-array";
import {View} from "tns-core-modules/ui/core/view";

let name = ["Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic",
    "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary", "Ireland", "Italy",
    "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania", "Slovakia",
    "Slovenia", "Spain", "Sweden", "United Kingdom"]

let address = ["Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic",
    "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary", "Ireland", "Italy",
    "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania", "Slovakia",
    "Slovenia", "Spain", "Sweden", "United Kingdom"]

@Component({
    selector: 'ns-requests',
    templateUrl: './requests.component.html',
    styleUrls: ['./requests.component.css'],
    moduleId: module.id,
})
export class RequestsComponent implements OnInit {

    searchRequest: string
    selectedTab = 0
    private _numberOfAddedItems
    private _requests: ObservableArray<Request>
    private _sourceDataItems: ObservableArray<Request>
    private layout: ListViewLinearLayout
    private _selectedItems: string
    private leftThresholdPassed
    private rightThresholdPassed

    constructor(private _routerExtensions: RouterExtensions,
                private _changeDetectionRef: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.layout = new ListViewLinearLayout()
        this.layout.scrollDirection = ListViewScrollDirection.Vertical
        this.initRequestList()
        this._changeDetectionRef.detectChanges()
        this._requests = new ObservableArray<Request>()
        this.addMoreItemsFromSource(4)
        this._selectedItems = "Nenhum item selecionando"
    }

    getAllRequests() {
        this.selectedTab = 0
    }

    getOpenRequests() {
        this.selectedTab = 1
    }

    getProgressRequests() {
        this.selectedTab = 2
    }

    getCloseRequests() {
        this.selectedTab = 3
    }

    public get requests(): ObservableArray<Request> {
        return this._requests
    }

    showRequestDetail(id: number) {
        this._routerExtensions.navigate(['pages/request/'+ id], {
            animated: true,
            transition: {
                name: 'slideLeft',
                duration: 380,
                curve: 'easeIn'
            }
        })
    }

    get selectedItems(): string {
        return this._selectedItems
    }

    onItemSelected(args: ListViewEventData) {
        const listView = args.object as RadListView
        const selectedItem = this.requests.getItem(args.index)
        console.log(selectedItem)
        // ABRIR MODAL PARTE DE BAIXO COM OPÇÕES
        alert("item selecionando")
    }

    onItemDeselected(args: ListViewEventData) {
        const listView = args.object as RadListView
        const deselectedItem = this.requests.getItem(args.index)
        console.log(deselectedItem)
        alert('deselecionandoe')
    }

    addMoreItemsFromSource(chunkSize: number) {
        let newRequest = this._sourceDataItems.splice(0,chunkSize)
        this.requests.push(newRequest)
    }

    onLoadMoreItemsRequested(args: LoadOnDemandListViewEventData) {
        const that = new WeakRef(this)
        const listView: RadListView = args.object

        if (this._sourceDataItems.length > 0) {
            setTimeout(() => {
                that.get().addMoreItemsFromSource(2)
                listView.notifyLoadOnDemandFinished()
            },1500)
        } else {
            args.returnValue = false
            listView.notifyLoadOnDemandFinished(true)
        }
    }

    onPullToRefreshList(args: ListViewEventData) {
        const that = new WeakRef(this)
        setTimeout(() => {
            const initialNumberOfItems = that.get()._numberOfAddedItems
            for(let i = that.get()._numberOfAddedItems; i < initialNumberOfItems + 2; i++) {
                if (i > name.length - 1) {
                    break
                }
                that.get()._requests.splice(0, 0, new Request(i, name[i], address[i], true) )
                that.get()._numberOfAddedItems++
            }
            const listView = args.object
            listView.notifyPullToRefreshFinished()
        },1000)
    }

    onSwipeCellStarted(args: ListViewEventData) {
        const swipeLimits = args.data.swipeLimits;
        const swipeView = args['object'];
        const leftItem = swipeView.getViewById('mark-view');
        const rightItem = swipeView.getViewById('delete-view');
        swipeLimits.left = swipeLimits.right = args.data.x > 0 ? swipeView.getMeasuredWidth() / 2 : swipeView.getMeasuredWidth() / 2;
        swipeLimits.threshold = swipeView.getMeasuredWidth();
    }

    // public onCellSwiping(args: ListViewEventData) {
    //     const swipeLimits = args.data.swipeLimits;
    //     const swipeView = args['swipeView'];
    //     const mainView = args['mainView'];
    //     const leftItem = swipeView.getViewById('mark-view');
    //     const rightItem = swipeView.getViewById('delete-view');
    //
    //     if (args.data.x > swipeView.getMeasuredWidth() / 4 && !this.leftThresholdPassed) {
    //         console.log("Notify perform left action");
    //         const markLabel = leftItem.getViewById('mark-text');
    //         this.leftThresholdPassed = true;
    //     } else if (args.data.x < -swipeView.getMeasuredWidth() / 4 && !this.rightThresholdPassed) {
    //         const deleteLabel = rightItem.getViewById('delete-text');
    //         console.log("Notify perform right action");
    //         this.rightThresholdPassed = true;
    //     }
    //     if (args.data.x > 0) {
    //         const leftDimensions = View.measureChild(
    //             leftItem.parent,
    //             leftItem,
    //             layout.makeMeasureSpec(Math.abs(args.data.x), layout.EXACTLY),
    //             layout.makeMeasureSpec(mainView.getMeasuredHeight(), layout.EXACTLY));
    //         View.layoutChild(leftItem.parent, leftItem, 0, 0, leftDimensions.measuredWidth, leftDimensions.measuredHeight);
    //     } else {
    //         const rightDimensions = View.measureChild(
    //             rightItem.parent,
    //             rightItem,
    //             layout.makeMeasureSpec(Math.abs(args.data.x), layout.EXACTLY),
    //             layout.makeMeasureSpec(mainView.getMeasuredHeight(), layout.EXACTLY));
    //
    //         View.layoutChild(rightItem.parent, rightItem, mainView.getMeasuredWidth() - rightDimensions.measuredWidth, 0, mainView.getMeasuredWidth(), rightDimensions.measuredHeight);
    //     }
    // }
    //
    // public onSwipeCellFinished(args: ListViewEventData) {
    //     const swipeView = args['object'];
    //     const leftItem = swipeView.getViewById('mark-view');
    //     const rightItem = swipeView.getViewById('delete-view');
    //     if (this.leftThresholdPassed) {
    //         console.log("Perform left action");
    //     } else if (this.rightThresholdPassed) {
    //         console.log("Perform right action");
    //     }
    //     this.leftThresholdPassed = false;
    //     this.rightThresholdPassed = false;
    // }

    private initRequestList() {
        //this._requests = new ObservableArray<Request>()
        this._sourceDataItems = new ObservableArray<Request>()
        this._numberOfAddedItems = 0
        for(let i = 0; i < name.length; i++) {
            this._numberOfAddedItems++
            this._sourceDataItems.push(new Request(i, name[i], address[i], true))
        }

    }

    onSetupItemView(args: SetupItemViewArgs) {
        args.view.context.third = (args.index % 3 === 0);
        args.view.context.header = ((args.index + 1) % name.length === 1);
        args.view.context.footer = (args.index + 1 === name.length);
    }

    onSubmit(args) {
        let searchBar = <SearchBar>args.object
        alert('valor da pesquisa ' + searchBar.text)
    }

    onTextChange(args) {
        let searchBar = <SearchBar>args.object;
        console.log("SearchBar text changed! New value: " + searchBar.text);
    }

    onClear(args) {
        let searchBar = <SearchBar>args.object
        searchBar.text = ''
        searchBar.hint = 'Pesquisar pedido de coleta'
        searchBar.dismissSoftInput()
    }

    onLoaded(args) {
        let searchBar = <SearchBar>args.object
        if (isAndroid)
            searchBar.android.clearFocus()
    }

    goBack() {
        this._routerExtensions.backToPreviousPage()
    }

}
