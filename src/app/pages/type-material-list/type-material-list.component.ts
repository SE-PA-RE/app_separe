import {Component, Input, OnInit} from '@angular/core';
import {TypeMaterial} from "~/app/shared/models/type-material.model";


@Component({
  selector: 'ns-type-material-list',
  templateUrl: './type-material-list.component.html',
  styleUrls: ['./type-material-list.component.css'],
  moduleId: module.id,
})
export class TypeMaterialListComponent implements OnInit {
    @Input() typeMaterial: Array<TypeMaterial> = []
    typeIcons: {}

  constructor() { }

  ngOnInit() {
        this.typeIcons = this.getTypeIcons()
  }

  public getBadgeIcon(id: string) {
        return this.typeIcons[id.toLowerCase()]
  }

  getTypeIcons() {
     return {
         plastico: '~/images/icon_plastico.png',
         madeira: '~/images/icon_madeira.png',
         papel: '~/images/icon_papel.png',
         saude: '~/images/icon_ambulatóriais_saude.png',
         organicos: '~/images/icon_organicos.png',
         metal: '~/images/icon_metal.png',
         vidro: '~/images/icon_vidro.png'
     }
  }
}
