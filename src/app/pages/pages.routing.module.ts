import {Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {NativeScriptRouterModule} from "nativescript-angular";

import {RequestsComponent} from "~/app/pages/requests/requests.component";
import {RequestDetailComponent} from "~/app/pages/request-detail/request-detail.component";
import {ProfileComponent} from "~/app/pages/profile/profile.component";
import {RequestCreateComponent} from "~/app/pages/request-create/request-create.component";
import {CollectionPointComponent} from "~/app/pages/collection-point/collection-point.component";


const routes: Routes = [
    { path: 'request/:id', component: RequestDetailComponent},
    { path: 'requests', component: RequestsComponent },
    { path: 'profile', component: ProfileComponent},
    { path: 'order/create', component: RequestCreateComponent},
    { path: 'points', component: CollectionPointComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})

export class PagesRoutingModule {}
