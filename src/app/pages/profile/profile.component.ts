import { Component, OnInit } from '@angular/core';
import {RouterExtensions} from "nativescript-angular";

@Component({
  selector: 'ns-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  moduleId: module.id,
})
export class ProfileComponent implements OnInit {

  constructor(private _routerExtensions: RouterExtensions) { }

  ngOnInit() {
  }

    goBack() {
      this._routerExtensions.back()
    }
}
