import {Component, OnInit} from "@angular/core";
import {Request} from "~/app/shared/models/request.model"
import {RouterExtensions} from "nativescript-angular";
import {ParamMap, ActivatedRoute} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {TypeMaterial} from "~/app/shared/models/type-material.model";

@Component({
    selector: 'ns-request-detail',
    templateUrl: './request-detail.html',
    styleUrls: ['./request-detail.css'],
    moduleId: module.id
})
export class RequestDetailComponent implements OnInit{
    requestId: number
    request: Request

    typeMaterial: Array<any> = [
        {id: 1, name: 'metal', description: 'Em química e tecnologia, os plásticos são materiais orgânicos poliméricos '},
        {id: 2, name: 'plástico', description: 'O papel é um material constituído por elementos fibrosos de origem vegetal'},
        {id: 3, name: 'papel', description: 'O papel é um material constituído por elementos fibrosos de origem vegetal'}
    ]

    constructor(private _routerExtensions: RouterExtensions,
                private _route: ActivatedRoute){}

    ngOnInit(): void {
        let id = this._route.snapshot.paramMap.get('id')
        this.requestId = parseInt(id)
        // this.request = this._route.paramMap.pipe(
        //     switchMap((params: ParamMap) =>
        //     this.service.getREquest(params.get('id')))
        // )
    }

    goBack() {
        this._routerExtensions.back()
    }
}
