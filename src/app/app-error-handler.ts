import {ErrorHandler, Injectable, NgZone} from "@angular/core";
import {HttpErrorResponse} from "@angular/common/http";
import {LoginService} from "~/app/services/login.service";

@Injectable()
export class AppErrorHandler extends ErrorHandler{

    constructor(private _loginService: LoginService,
                private _zone: NgZone){
        super()
    }

    handleError(error: any): void {
        if (error instanceof HttpErrorResponse){
            this._zone.run(() =>{
                console.log(error)
                switch (error.status) {
                     case 404:
                         alert(error.error.message)
                         break;
                    case 403:
                        alert(error.error.detail)
                    default:
                        alert("Ocorrou um error, tente novamente")
                 }
            })
        }
    }
}
