import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import { environment } from "../environments/environment";


@Injectable({
    providedIn: "root"
})
export class MaterialService {

    api: string = environment.api

    constructor(private _http: HttpClient){}

    getTypes(): Observable<any> {
        return this._http.get(`${this.api}types/`)
    }
}
