import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { CollectionDetail } from '../shared/models/collection-detail.model';

@Injectable({
    providedIn: 'root'
})
export class CollectionService {

    api: string = environment.api

    constructor(private _http: HttpClient){}

    addCollection(collection: CollectionDetail) {
        return this._http.post(`${this.api}collections/`, collection)
    }
}
