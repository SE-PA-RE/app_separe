import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Credential} from "~/app/shared/models/credential.model";
import {tap} from "rxjs/operators";
import {TokenService} from "~/app/services/token.service";
import {RouterExtensions} from "nativescript-angular";
import {Token} from "~/app/shared/models/token.model";
import { environment } from "../environments/environment";


@Injectable({
    providedIn: 'root'
})
export class LoginService{

    api: string = environment.api

    private credentials: Credential = {
        grant_type: 'password',
        username: '',
        password: '',
        client_id: environment.client_id,
        client_secret: environment.secret_id
    }

    constructor(private _http: HttpClient,
                private _routerExtensions: RouterExtensions,
                private _tokenService: TokenService){}


    getToken(email: string, password: string) {
        this.credentials.username = email
        this.credentials.password = password

        return this._http.post(`${this.api}token/`,this.credentials)
            .pipe(
                tap(
                    data => {
                        this._tokenService.setToken(data as Token)
                    }
                )
            )
    }

    /**
     * Sai do aplicativo e redireciona para login
     *
     * @memberof LoginService
     */
    logout() {
        this._tokenService.removeToken()
        this._routerExtensions.navigate(['login'], {
            clearHistory: true,
            animated: true,
            transition: {
                name: 'fade',
                duration: 500,
                curve: 'ease'
            }
        })
    }

    resetPassword(email: string) {
        let body = {'email': email}
        return this._http.post(`${this.api}users/reset_password/`, body)
    }

    isLoggedIn(): boolean {
        return !!this._tokenService.geToken()
    }
}
