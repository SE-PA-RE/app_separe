import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CityService {
    api: string = environment.api

    constructor(private _http: HttpClient){}

    getCities(){
        return this._http.get(`${this.api}cities/`)
    }
}
