import {Injectable} from "@angular/core";
import {getString, setString, clear} from "tns-core-modules/application-settings";
import {Token} from "~/app/shared/models/token.model";

@Injectable({
    providedIn: 'root'
})
export class TokenService {

    geToken(): string {
        return getString("token")
    }

    setToken(theToken: Token){
        let today = new Date()
        today.setSeconds(new Date().getSeconds() + 2592000)

        setString("token",theToken.access_token)
        setString("expires_in", today.toString())
        setString("refresh_token",theToken.refresh_token)
    }

    /**
     * Exclui o token salvo no aplicativo
     *
     * @memberof TokenService
     */
    removeToken(){
        clear() // Essa função apaga tudo que está amazenado no aplicativo
    }

}
