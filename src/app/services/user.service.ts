import {Injectable} from "@angular/core";
import {EcoFriend} from "~/app/shared/models/eco-friend.model";
import {HttpClient} from "@angular/common/http";
import {EcoRecycler} from "~/app/shared/models/eco-recycler.model";
import { environment } from "../environments/environment";


@Injectable({
    providedIn: 'root'
})
export class UserService {

    api: string = environment.api

    constructor(private _http: HttpClient){}

    addEcoFriend(friend: EcoFriend) {
        return this._http.post(`${this.api}friends/`, friend)
    }

    addEcoRecycler(recycler: EcoRecycler) {
        return this._http.post(`${this.api}recyclers/`, recycler)
    }

    getMyProfile() {
        return this._http.get(`${this.api}users/get_profile/`)
    }

    getMyAddress() {
        return this._http.get(`${this.api}users/get_address/`)
    }
}
