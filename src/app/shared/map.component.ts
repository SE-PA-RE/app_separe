import {Component, Input} from "@angular/core";
import {registerElement} from "nativescript-angular/element-registry";
import { MapView, Marker, Position} from 'nativescript-google-maps-sdk';

registerElement('MapView', () => MapView);

import {Location} from "./models/location.model"

// Esse componente não está sendo usando no projeto, será no futuro

@Component({
    selector: 'ns-map',
    template: `
        <GridLayout>
            <MapView #mapView 
                     [latitude]="latitude" [logintude]="longitude"
                     [zoom]="zoom" [minZoom]="minZoom" [maxZoom]="maxZoom"
                     [bearing]="bearing" [tilt]="tilt" 
                     i-padding="50,50,50,50" [padding]="padding" (mapReady)="onMapReady($event)"
            
                     (markerSelect)="onMarkerEvent($event)"
                     (coordinateTapped)="onCoordinateTapped($event)"
                     (cameraChanged)="onCameraChanged($event)"
                     (cameraMove)="onCameraMove($event)"                     
            ></MapView>
        </GridLayout>
    `,
    styles: [`
    `],
    moduleId: module.id
})
export class MapComponent {

    @Input() latitude: number = -36.799441
    @Input() longitude: number = 10.178554
    zoom = 10
    minZoom = 0
    maxZoom = 22
    bearing = 0
    tilt = 0
    padding = [40, 40, 40, 40]

    @Input() location: Location[] = []
    mapView: MapView

    lastCamera: String

    constructor(){}

    // Chamado quando o Google Map está pronto para uso
    onMapReady(args) {
        console.log('Map Ready')

        this.mapView = args.object;
        console.log("Setting a marker...");

        // se a bússola está habilitada / desabilitada
        this.mapView.settings.compassEnabled = true
        // Se os controles de zoom estão ativados / desativados ** Apenas Android **
        this.mapView.settings.zoomControlsEnabled = true
        // se o botão my-location está ativado / desativado
        this.mapView.settings.myLocationButtonEnabled = false


        var marker = new Marker();
        marker.position = Position.positionFromLatLng(36.72075940812857, 10.224237393346227);
        marker.title = "kkkkk";
        marker.snippet = "aaaa";
        marker.userData = {index: -1};
        console.log(marker)
        this.mapView.addMarker(marker)


    }

    // Dispara sempre que um marcador é selecionado
    onMarkerEvent(args) {
        console.log("Marker Event: '" + args.eventName
            + "' triggered on: " + args.marker.title
            + ", Lat: " + args.marker.position.latitude + ", Lon: " + args.marker.position.longitude, args);
    }

    // Dispara quando a coordenada é clicada no mapa
    onCoordinateTapped(args) {
        console.log("Coordinate Tapped, Lat: " + args.position.latitude + ", Lon: " + args.position.longitude, args);
    }

    // Disparou depois que a câmera mudou
    onCameraChanged(args) {
        console.log("Camera changed: " + JSON.stringify(args.camera), JSON.stringify(args.camera) === this.lastCamera);
        this.lastCamera = JSON.stringify(args.camera);
    }

    // Disparado enquanto a câmera está se movendo
    onCameraMove(args) {
        console.log("Camera moving: " + JSON.stringify(args.camera));
    }


    generateRandomPosition(position, distance) {
        var r = distance / 111300;

        var x = position[0];
        var y = position[1];

        var u = Math.random();
        var v = Math.random();

        var w = r * Math.sqrt(u);
        var t = 2 * Math.PI * v;

        var dx = w * Math.cos(t) / Math.cos(y);
        var xy = w * Math.sin(t);

        return [x + dx, y + xy];
    }
}
