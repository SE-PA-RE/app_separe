export interface Token {
    access_token: string
    expires_in: string
    token_type: string
    scope: string
    refresh_token: string
}
