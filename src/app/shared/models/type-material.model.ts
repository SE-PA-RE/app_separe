export class TypeMaterial {
    id: number
    type: string
    description: string
}
