export class Request {
    constructor(
      public id: number,
      public name: string,
      public address: string,
      public isLike: boolean
    ){}
}
