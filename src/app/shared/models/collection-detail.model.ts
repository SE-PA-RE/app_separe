import { TypeMaterial } from "./type-material.model";
import { Address } from "./address.model";
import { Location } from "./location.model";

export interface CollectionDetail{
    collects: TypeMaterial[]
    location: Location
    address: Address
    image: string    
}