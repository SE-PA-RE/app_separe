export interface EcoRecycler {
    first_name: string
    last_name: string
    email: string
    cpf: string
    date_of_birth: string
    phone: string
    password: string
    document: string
    declaration: string
}
