export class Address {
    logradouro: string
    description: string
    number: string
    sector: string
    city: string
    uf: string
    cep: string
}
