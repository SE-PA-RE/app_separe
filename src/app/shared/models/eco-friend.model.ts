export interface EcoFriend {
    first_name: string
    last_name: string
    email: string
    cpf: string
    date_of_birth: string
    phone: string
    password: string
}

export interface EcoFriendValidator {
    first_name: Array<string>
    last_name: Array<string>
    email: Array<string>
    cpf: Array<string>
    date_of_birth: Array<string>
    phone: Array<string>
    password: Array<string>
    document: Array<string>
    declaration: Array<string>
}
