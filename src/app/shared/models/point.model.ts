import {Location} from "~/app/shared/models/location.model";

export interface Point {
    id: number
    name: string
    description: string
    location: Location
}
