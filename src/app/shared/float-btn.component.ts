import {Component, Output, Input, EventEmitter} from "@angular/core";
import {TouchGestureEventData, GestureEventData} from "tns-core-modules/ui/gestures";
import {Color} from "tns-core-modules/color";
import {device} from "tns-core-modules/platform";

declare const CGSizeMake: any
declare const android: any

@Component({
    selector: 'float-btn',
    template: `
       <GridLayout class="float-btn-wrapper">
           <StackLayout class="float-btn-shadow" (loaded)="onLoaded($event)">
               <StackLayout class="float-btn" (touch)="onTouch($event)"  (tap)="onTap($event)">
                   <Label class="float-btn-text" text="+"></Label>
               </StackLayout>
           </StackLayout>
       </GridLayout>
    `,
    styles: [
        `    
            .float-btn-wrapper {
                width: 90;
                height: 90;                
            }
            .float-btn-shadow {
                width: 56;
                height: 56;
            }
            .float-btn {
                /*background-color: #30bcff;*/
                background-color: #b72806;
                border-radius: 28;
                width: 56;
                height: 56;
                text-align: center;
                vertical-align: middle;
            }   
            .float-btn-text {
                color: #ffffff;
                font-size: 36;
                margin-top: -4;
            }
        `
    ]
})
export class FloatBtnComponent {
    @Input() text: string = '+'
    @Output() tap: EventEmitter<GestureEventData> = new EventEmitter<GestureEventData>()
    @Output() touch: EventEmitter<TouchGestureEventData> = new EventEmitter<TouchGestureEventData>()

    onTap(args: GestureEventData) {
        this.tap.emit(args)
    }

    onTouch(args: TouchGestureEventData) {
        this.touch.emit(args)
    }

    onLoaded(args) {
        let tnsView = <any>args.object

        
        setTimeout(() => {
            tnsView.clipToBoungs = false
            if (tnsView.ios)
                tnsView.ios.clipsToBounds = false
        }, 200)

         if (tnsView.android && parseFloat(device.osVersion) >= 5.0) {
             let nativeAnView = tnsView.android
             var shape = new android.graphics.drawable.GradientDrawable()
             shape.setShape(android.graphics.drawable.GradientDrawable.OVAL)
             shape.setColor(android.graphics.Color.parseColor("#30bcff"))
             nativeAnView.setBackgroundDrawable(shape)
             nativeAnView.setElevation(20)

         } else if (tnsView.ios) {
             let nativeView = tnsView.ios
             nativeView.layer.shadowColor = this.shadowColor.ios.CGColor
             nativeView.layer.shadowOffset = CGSizeMake(0, this.shadowOffset)
             nativeView.layer.shadowOpacity = 0.5
             nativeView.layer.shadowRadius = 5.0
         }
    }

    private get shadowColor(): Color {
        return new Color('#888888')
    }

    private get shadowOffset(): number {
        return 2.0
    }
}
