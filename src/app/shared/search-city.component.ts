import { Component, OnInit } from '@angular/core';
import { City } from './models/city.model';
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';
import { ModalDialogParams, RouterExtensions } from 'nativescript-angular';
import {isAndroid, Page} from 'tns-core-modules/ui/page/page';
import { ActivatedRoute } from '@angular/router';
import { SearchBar } from 'tns-core-modules/ui/search-bar/search-bar';
import { CityService } from '../services/city.services';

@Component({
    selector: 'ns-search-city',
    template: `
        <GridLayout rows="48,*">
            <SearchBar row="0" id="searchBar" hint="Procurar uma cidade" class="search" (loaded)="onLoaded($event)"
             (textChange)="onTextChanged($event)" 
             (clear)="onClear($event)" 
             (submit)="onSubmit($event)"></SearchBar>
            <ListView row="1" [items]="cities" (itemTap)="onSelectItem($event)" class="list-group">
                <ng-template let-item="item">
                  <StackLayout class="item list-group-item">
                    <Label [text]="item.name + ' - ' + item.uf" class="list-group-item-heading"></Label>
                  </StackLayout>
                </ng-template>
            </ListView>
        </GridLayout>
    `,
    moduleId: module.id
})
export class SearchCityComponent implements OnInit {
    private _searchedText: string = ''
    private arrayCities: Array<City> = []
    public cities: ObservableArray<City> = new ObservableArray<City>()

    constructor(private _params: ModalDialogParams,
                private _page: Page,
                private _router: RouterExtensions,
                private _activeRoute: ActivatedRoute,
                private _cityService: CityService) { }

    ngOnInit(): void {
        this._cityService.getCities()
            .subscribe(result => {
                this.arrayCities = result['data']
                this.cities.push(this.arrayCities)
            })
     }

    onClose(): void {
        this._params.closeCallback("Cidade")
    }

    onSelectItem(args) {
        let city = (this._searchedText !== '') ? this.cities.getItem(args.index) : this.arrayCities[args.index]
        this._params.closeCallback({
            city
        })
    }

    public onSubmit(args) {
        let searchBar = <SearchBar> args.object
        let searchValue = searchBar.text.toLowerCase()
        this._searchedText = searchValue

        this.cities = new ObservableArray<City>()
        if (searchValue !== "") [
            console.log(searchValue)
        ]
    }

    onLoaded(args) {
        let searchBar = <SearchBar>args.object
        searchBar.dismissSoftInput
        if (isAndroid)
            searchBar.android.clearFocus()
    }

    public onClear(args) {
        let searchBar = <SearchBar> args.object
        searchBar.text = ''
        searchBar.hint = 'Procurar uma cidade'
        this.cities = new ObservableArray<City>()
    }

    public onTextChanged(args) {
        this.onSubmit(args)
    }
}
