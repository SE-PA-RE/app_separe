import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {LoginService} from "~/app/services/login.service";

@Injectable({
  providedIn: 'root'
})
export class AppAuthGuard implements CanActivate{

  constructor(private router: Router,
              private _loginService: LoginService) { }

    canActivate() {
      if (this._loginService.isLoggedIn()) {
          return true
      } else {
          this.router.navigate(['/login'])
          return false
      }
    }
}
